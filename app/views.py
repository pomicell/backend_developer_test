from app import app
from flask import render_template
from flask import request

@app.route('/')
def index():
    return render_template("index.html")


@app.route('/load-protein')
def load_protein():
    protein_id = request.args.get('proteinId')

    # Enter your code here

    return protein_id